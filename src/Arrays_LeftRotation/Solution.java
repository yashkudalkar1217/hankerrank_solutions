
import java.util.Scanner;

public class Solution {

	/*Function to left rotate arr[] of size n by d*/
	void leftRotate(int arr[], int d, int n) 
	{
		int i;
		for (i = 0; i < d; i++)
			leftRotatebyOne(arr, n);
	}

	void leftRotatebyOne(int arr[], int n) 
	{
		int i, temp;
		temp = arr[0];
		for (i = 0; i < n - 1; i++)
			arr[i] = arr[i + 1];
		arr[i] = temp;
	}

	public static int[] arrayLeftRotation(int[] a, int n, int k) {

		for(int i=0;i<k;i++){
		rotation(a,n);
		}
		
		return a;

	}

	private static void rotation(int[] a, int n) {
		int i;
		int temp=a[0];
		for(i=0;i<n-1;i++){
			a[i]=a[i+1];
		}
		a[i]=temp;
		
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = in.nextInt();
		int a[] = new int[n];
		for(int a_i=0; a_i < n; a_i++){
			a[a_i] = in.nextInt();
		}

		int[] output = new int[n];
		output = arrayLeftRotation(a, n, k);
		for(int i = 0; i < n; i++)
			System.out.print(output[i] + " ");

		System.out.println();

	}
}
